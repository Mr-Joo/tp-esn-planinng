﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using System.Text;

namespace ERPBackEnd.Models
{
    public class Devis
    {
        public int Id { get; set; }
        public string N { get; set; }
        public string Prospect { get; set; }
        public string Typologie { get; set; }
        public int Charge { get; set; }
        public DateTime DebutDemande { get; set; }
        public DateTime FinDemande { get; set; }
        public string Statut { get; set; }
        public float Chance { get; set; }
        public string DateDeFaisabilite { get; set; }
		
		
		
    }
}
