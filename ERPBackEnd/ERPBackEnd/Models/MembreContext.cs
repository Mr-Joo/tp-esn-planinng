﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERPBackEnd.Models
{
    public class MembreContext : DbContext
    {
        public MembreContext(DbContextOptions<MembreContext> options) : base(options)
        {

        }
        public DbSet<Membre> Membre { get; set; }

    }
}
