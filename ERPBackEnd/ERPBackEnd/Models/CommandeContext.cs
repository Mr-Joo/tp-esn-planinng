﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ERPBackEnd.Models
{
    public class CommandeContext : DbContext
    {
        public CommandeContext(DbContextOptions<CommandeContext> options) : base(options)
        {

        }
        public DbSet<Commande> Commande { get; set; }
    }
}
