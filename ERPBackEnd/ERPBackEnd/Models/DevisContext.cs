﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERPBackEnd.Models
{
    public class DevisContext : DbContext
    {

        public DevisContext(DbContextOptions<DevisContext> options) : base(options)
        {

        }
        public DbSet<Devis> Devis { get; set; }
    }
}
