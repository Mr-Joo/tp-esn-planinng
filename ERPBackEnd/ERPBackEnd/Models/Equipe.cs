﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERPBackEnd.Models
{
    public class Equipe
    {
        public List<Membre> Membres { get; set; }
    }
}
