﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ERPBackEnd.Models;

namespace ERPBackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommandeController : ControllerBase
    {
        private readonly CommandeContext _context;

        public CommandeController(CommandeContext context)
        {
            _context = context;
        }

        // GET: api/Commande
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Commande>>> GetCommandes()
        {
            return await _context.Commande.ToListAsync();
        }

        // GET: api/Commande/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Commande>> GetCommande(int id)
        {
            var commande = await _context.Commande.FindAsync(id);

            if (commande == null)
            {
                return NotFound();
            }

            return commande;
        }

        // PUT: api/Commande/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCommande(int id, Commande commande)
        {
            if (id != commande.Id)
            {
                return BadRequest();
            }

            _context.Entry(commande).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CommandeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Commande
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Commande>> PostCommande(Commande commande)
        {
			
			int delai = commande.Charge;
			foreach( Commande com in _context.Commande)
			{
				delai += com.Charge;
			}
			DateTime dateDebut = commande.DebutDemande;
			int nbJourAjoute = 0;
            int i = 0;
            while (i <= delai)
			{
				string dateString = dateDebut.AddDays(nbJourAjoute).ToString("F", new System.Globalization.CultureInfo("fr-FR"));
				string jourDate = dateString.Split(' ').First();

				if( ( jourDate != "samedi" ) && ( jourDate != "dimanche" ) )
				{
					i++;
				}
				nbJourAjoute++;
			}
			DateTime dateFin = DateTime.Now.AddDays(nbJourAjoute);
			int result = DateTime.Compare( commande.FinDemande, dateFin );
			if( ( result < 0 ) || ( result == 0 ) )
			{
				commande.DateDeFaisabilite = dateFin.ToShortDateString();
			}
			else 
			{
				commande.DateDeFaisabilite = commande.FinDemande.ToShortDateString();
			}

			_context.Commande.Add(commande);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetCommande", new { id = commande.Id }, commande);
        }

        // DELETE: api/Commande/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Commande>> DeleteCommande(int id)
        {
            var commande = await _context.Commande.FindAsync(id);
            if (commande == null)
            {
                return NotFound();
            }

            _context.Commande.Remove(commande);
            await _context.SaveChangesAsync();

            return commande;
        }

        private bool CommandeExists(int id)
        {
            return _context.Commande.Any(e => e.Id == id);
        }
    }
}
