﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ERPBackEnd.Models;

namespace ERPBackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MembreController : ControllerBase
    {
        private readonly MembreContext _context;

        public MembreController(MembreContext context)
        {
            _context = context;
        }

        // GET: api/Membre
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Membre>>> GetMembres()
        {
            return await _context.Membre.ToListAsync();
        }

        // GET: api/Membre/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Membre>> GetMembre(int id)
        {
            var membre = await _context.Membre.FindAsync(id);

            if (membre == null)
            {
                return NotFound();
            }

            return membre;
        }

        // PUT: api/Membre/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMembre(int id, Membre membre)
        {
            if (id != membre.Id)
            {
                return BadRequest();
            }

            _context.Entry(membre).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MembreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Membre
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Membre>> PostMembre(Membre membre)
        {
            _context.Membre.Add(membre);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMembre", new { id = membre.Id }, membre);
        }

        // DELETE: api/Membre/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Membre>> DeleteMembre(int id)
        {
            var membre = await _context.Membre.FindAsync(id);
            if (membre == null)
            {
                return NotFound();
            }

            _context.Membre.Remove(membre);
            await _context.SaveChangesAsync();

            return membre;
        }

        private bool MembreExists(int id)
        {
            return _context.Membre.Any(e => e.Id == id);
        }
    }
}
